class_name Pipe
extends Area2D


export var speed: int = 160


func _process(delta: float) -> void:
	position.x -= delta * speed


func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()
