# Freight Snake
A Flappy Bird clone with simple colored rectangles.

Released under the MIT license.

Download the latest build from [itch.io](https://bfedie518.itch.io/flippy-flap).

